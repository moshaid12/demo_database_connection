
<?php
$id = $_GET['id'];


// Database Connection
$servername = "localhost";
$username = "root";
$password = "";

// try {
$pdo = new PDO("mysql:host=$servername;dbname=db_php_b9_2023", $username, $password);
// $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// echo "Connected successfully";
// } catch(PDOException $e) {
// echo "Connection failed: " . $e->getMessage();
// }

$stmt = $pdo->query("SELECT * FROM categories WHERE id = $id");
 $category = $stmt->fetch();






?>


<!DOCTYPE html>
<html>
 <?php include_once('../inc/partials/head.php')?>
  <body id="body-pd">
  <?php include_once('../inc/partials/header.php')?>
  <?php include_once('../inc/partials/sidebar.php')?>
    
    <div class="height-100 bg-light">
      <h4>Category Update | <span><a href="./index.php">List</a></span></h4>
        <div class="card">
            <div class="card-body">
            <form action="./update-store.php" method="POST">
                <div class="mb-3">
                  <input type="hidden" class="form-control" name="id" id="id"  value="<?php echo $id;?>">
                  <label for="name" class="form-label">Name</label>
                  <input type="text" class="form-control" name="name" id="name" aria-describedby="emailHelp" value="<?php echo $category['name'];?>">
                  <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
                </div>
                <div class="mb-3">
                  <label for="description" class="form-label">Description</label>
                  <input type="text" class="form-control" name="description" id="description" value="<?php echo $category['description'];?>">
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
            </div>
        </div>
    </div>
  <?php include_once('../inc/partials/footer.php')?>
</body>
</html>
