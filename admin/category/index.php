<?php 
// session_start();

// $_SESSION['name'] = $_POST['name']?? "";
// $_SESSION['description'] = $_POST['description']?? "";

 // Database Connection
 $servername = "localhost";
 $username = "root";
 $password = "";

 // try {
 $pdo = new PDO("mysql:host=$servername;dbname=db_php_b9_2023", $username, $password);
 // $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 // echo "Connected successfully";
 // } catch(PDOException $e) {
 // echo "Connection failed: " . $e->getMessage();
 // }

// Query korte hobe

$stmt = $pdo->query("SELECT * FROM categories");
$categories = $stmt->fetchAll();
// echo "<pre>";
// print_r($categories);
// echo "</pre>";

// Data show korte hobe


?>
<!DOCTYPE html>
<html>
 <?php include_once('../inc/partials/head.php')?>
  <body id="body-pd">
  <?php include_once('../inc/partials/header.php')?>
  <?php include_once('../inc/partials/sidebar.php')?>
    
    <div class="height-100 bg-light">
      <h4>Category List </h4>
        <div class="card">
          <div class="card-body">
             <a class="btn btn-primary" href="./create.php">Create</a>
                <table id="example" class="table table-striped" style="width:100%">
                  <thead>
                      <tr>
                          <th>Ser No</th>
                          <th>Name</th>
                          <th>Description</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                     <?php 
                     $i = 1;
                     
                     
                     foreach($categories as $category){ ?>

                     

                      <tr>
                          <td><?= $i++;?></td>
                          <td><?php echo $category['name'];?></td>
                          <td><?php echo $category['description'];?></td>
                          <td>
                              <a href="./show.php?id=<?php echo $category['id']?>">Show</a>|
                              <a href="./edit.php?id=<?php echo $category['id']?>">Edit</a>|
                              <a href="" onclick="confirm('Are you Sure?')" >Delete</a>
                          </td>
                        
                      </tr>

                     <?php }
                     
                     ?>
                      
                  </tbody>
                  </table>
          </div>
        </div>
    </div>
    <?php include_once('../inc/partials/footer.php')?>
  </body>
</html>
