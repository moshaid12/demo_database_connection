<!DOCTYPE html>
<html>
 <?php include_once('../inc/partials/head.php')?>
  <body id="body-pd">
  <?php include_once('../inc/partials/header.php')?>
  <?php include_once('../inc/partials/sidebar.php')?>
    
    <div class="height-100 bg-light">
      <h4>Category Create | <span><a href="./index.php">List</a></span></h4>
        <div class="card">
            <div class="card-body">
            <form action="./store.php" method="POST">
              <div class="mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" name="name" class="form-control" id="name" aria-describedby="emailHelp">
                <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
              </div>
              <div class="mb-3">
                <label for="description" class="form-label">Description</label>
                <input type="text" name="description" class="form-control" id="description">
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            </div>
        </div>
    </div>
  <?php include_once('../inc/partials/footer.php')?>
</body>
</html>
